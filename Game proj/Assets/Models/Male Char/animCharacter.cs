using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animCharacter : MonoBehaviour
{

    public GameObject character;

    void Update()
    {
        if (Input.GetButtonDown("1key_idle"))
        {
            character.GetComponent<Animator>().Play("Idle");
        }
        if (Input.GetButtonDown("2key_slow_run"))
        {
            character.GetComponent<Animator>().Play("Slow Run");
            character.GetComponent<Animator>().Play("Run");
        }
        if (Input.GetButtonDown("3key_cheer"))
        {
            character.GetComponent<Animator>().Play("Cheer");
        }
        if (Input.GetButtonDown("4key_die"))
        {
            character.GetComponent<Animator>().Play("Die");
        }
    }

}
