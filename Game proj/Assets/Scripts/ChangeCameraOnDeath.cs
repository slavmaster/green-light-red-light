using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeCameraOnDeath : MonoBehaviour
{ 
    private GameObject _other;
    private GameObject _player;
    private GameObject _doll;
    private GameObject _loserCamera;
    private float _turnedDistance;
    void Start()
    {
        _turnedDistance = 999;
        _other = GameObject.Find("Finish_Line");
        _player = GameObject.Find("/XR Origin/Camera Offset/Main Camera");
        _doll = GameObject.Find("Doll");
        _loserCamera = GameObject.Find("OtherCamera");
    }

    void Update()
    {
        var dist = Vector3.Distance(_other.transform.position, _player.transform.position);
        if (_doll.GetComponent<Animator>().GetBool("isRed") && Mathf.Approximately(_turnedDistance,999))
        {
            _turnedDistance = dist;
            //Debug.Log("Doll turned and player was at " + _turnedDistance);

        }
        float finalDistance  = Mathf.Abs(_other.transform.position.z - _player.transform.position.z);

        if (finalDistance <= 0.09)
        {
            SceneManager.LoadScene("Leaderboards");
        }
        if (Mathf.Approximately(_turnedDistance,999)==false)
            if (Mathf.Approximately(_turnedDistance,dist) == false)
            {
                Debug.Log("You died.");
               // _player.GetComponent<Animator>().Play("Die");
                _player.SetActive(false);
                _loserCamera.transform.GetChild (0).gameObject.SetActive(true);
            }
        
        if (_doll.GetComponent<Animator>().GetBool("isRed") == false)
            _turnedDistance = 999;
    }
}
