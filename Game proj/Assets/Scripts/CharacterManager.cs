using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterManager : MonoBehaviour
{
 
    public SpriteRenderer spriteToChange;
    public Image imageToChange;
    public Sprite male;
    public Sprite female;
    private bool isMale = true;

    public void Start()
    {
        spriteToChange.sprite = female;
    }

    public void ChangeOption()
    {

        if (isMale)
        {
            UpdateCharacter(female);
            isMale = false;
        }
        else
        {
            UpdateCharacter(male);
            isMale = true;
        }
   
    }

    private void UpdateCharacter(Sprite characterToUpdate)
    {
        imageToChange.sprite = characterToUpdate;
        spriteToChange.sprite = characterToUpdate;
    }

    public void Save() 
    {
        SceneManager.LoadScene("Main menu");
    }
}
