using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConnectToServer : MonoBehaviourPunCallbacks
{ 
    void Start()
    {
        QualitySettings.vSyncCount = 1;
        PhotonNetwork.ConnectUsingSettings();
        Debug.Log("Trying to connect.");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to server");
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        SceneManager.LoadScene("Main menu");
    }
    
    // public override void OnPlayerEnteredRoom(Player newPlayer)
    // {
    //     Debug.Log("A new player entered the room.");
    //     base.OnPlayerEnteredRoom(newPlayer);
    // }
}