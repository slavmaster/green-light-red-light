using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CreateAndJoin : MonoBehaviourPunCallbacks
{
    public TMP_InputField roomToCreate;
    public TMP_InputField codeToJoin;

    public void CreateRoom()
    {
        Debug.Log("Created room " + roomToCreate.text);
        PhotonNetwork.CreateRoom(roomToCreate.text);
    }

    public void JoinRoom()
    {
        Debug.Log("Joining room" + codeToJoin.text);
        PhotonNetwork.JoinRoom(codeToJoin.text);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Joined a room.");
        PhotonNetwork.LoadLevel("WaitingScene");    
    }

    // public override void OnPlayerEnteredRoom(Player newPlayer)
    // {
    //     Debug.Log("A new player entered the room.");
    //     base.OnPlayerEnteredRoom(newPlayer);
    // }
}