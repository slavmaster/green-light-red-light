using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    [Header("Game to load")] 
    public string newGame;
    
    public void StartGame()
    {
        SceneManager.LoadScene(newGame);
    }

    public void Settings() {
        SceneManager.LoadScene("CharSelection");
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
