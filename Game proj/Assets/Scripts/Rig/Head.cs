using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Head : MonoBehaviour
{
    [SerializeField] private Transform rootObject, followObject;
    [SerializeField] private Vector3 positionOffset, rotationOffset, headBodyOffset;
    private PhotonView view;
    
    void Start()
    {
        if (Camera.main != null) followObject = Camera.main.transform;
        view = GetComponentInParent<PhotonView>();
    }
    
    void LateUpdate()
    {
        if (view.IsMine)
        {
            rootObject.position = transform.position + headBodyOffset;
            rootObject.forward = Vector3.ProjectOnPlane(followObject.up, Vector3.up).normalized;

            transform.position = followObject.TransformPoint(positionOffset);
            transform.rotation = followObject.rotation * Quaternion.Euler(rotationOffset);
        }
    }
}
