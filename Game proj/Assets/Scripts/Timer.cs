using TMPro;
using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(PhotonView))]
public class Timer : MonoBehaviour, IPunObservable
{
    // Start is called before the first frame update
    public GameObject timerText;
    public float timeRemaining;
    public bool timerIsRunning;
    public float timeBeforeStart;
    public AudioClip naratorSound;
    public AudioSource audioSource;
    public float volume = 0.5f;
    private bool wasPlayed = false;

    public static void MyDelay(int seconds)
    {
        DateTime ts = DateTime.Now + TimeSpan.FromSeconds(seconds);

        do { } while (DateTime.Now < ts);
    }

    public void OnPhotonSerializeView(PhotonStream stream,PhotonMessageInfo info)
    {

        if (timeBeforeStart <= 0)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(timeRemaining);
            }
            else if (stream.IsReading)
            {
                timeRemaining = ((float)stream.ReceiveNext());
            }
        }

    }

    void DisplayTime(float timeToDisplay)
    {
        if (timeBeforeStart <= 0)
        {
            timeToDisplay += 1;

            float minutes = Mathf.FloorToInt(timeToDisplay / 60);
            float seconds = Mathf.FloorToInt(timeToDisplay % 60);
            timerText.GetComponent<TextMeshPro>().text = $"{minutes:00}:{seconds:00}";
        }
    }

    void Update()
    {
            timeBeforeStart = timeBeforeStart - ((float)(1));

            if (timeBeforeStart <= 0)
            {

                if (timerIsRunning)
                {
                    if (timeRemaining > 0)
                    {
                        timeRemaining -= Time.deltaTime;
                        DisplayTime(timeRemaining);
                    }
                    else
                    {
                        Debug.Log("Time has run out!");
                        timeRemaining = 0;
                        timerIsRunning = false;
                    }
                }
            }
    }
}
