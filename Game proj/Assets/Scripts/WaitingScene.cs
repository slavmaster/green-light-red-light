using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class WaitingScene : MonoBehaviour
{
    int playerNumber;

    void Start()
    {
        playerNumber = PhotonNetwork.CurrentRoom.PlayerCount;

    }

    // Update is called once per frame
    void Update()
    {
        playerNumber = PhotonNetwork.CurrentRoom.PlayerCount; 

        if (playerNumber > 1)
        {
            PhotonNetwork.LoadLevel("GameScene");
        }
    }
    
}
