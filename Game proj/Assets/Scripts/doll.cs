using Photon.Pun;
using UnityEngine;

public class doll : MonoBehaviour, IPunObservable
{
    public GameObject dool;
    private Animator _animator;
    private float _timeForIdleBack = 5;
    private float _timeForIdleFront = 8;
    public AudioClip greenLightSound;
    public AudioClip redLightSound;
    public AudioSource audioSource;
    public float volume = 0.5f;
    private bool wasPlayedRed = false;
    private bool wasPlayedGreen = false;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_timeForIdleBack);
            stream.SendNext(_timeForIdleFront);
        }else if (stream.IsReading)
        {
            _timeForIdleBack = ((float) stream.ReceiveNext());
            _timeForIdleFront = ((float) stream.ReceiveNext());
        }
    }
    
    void Start()
    {
        _animator = dool.GetComponent<Animator>();
        _animator.SetBool("isRed", false);
    }
    void Update()
    {
        if (_timeForIdleBack <= 0)
        {
            _animator.SetBool("isRed", true);

            playSounds("red");

            if (_timeForIdleFront <= 0)
            {
                _animator.SetBool("isRed", false);
                playSounds("green");

                _timeForIdleBack = Random.Range(20,25);
                _timeForIdleFront = Random.Range(1,2);
            }
            else
            {
                _timeForIdleFront -= Time.deltaTime;
            }
        }
        else
        {
            _timeForIdleBack -= Time.deltaTime;
        }
    }

    public void playSounds(string sound) {
        if (!wasPlayedRed)
        {
            if (sound == "red") {
                audioSource.PlayOneShot(redLightSound, volume);
                wasPlayedRed = true;
                wasPlayedGreen = false;
            }
            
        }
        if (!wasPlayedGreen)
        {
            if (sound == "green")
            {
                audioSource.PlayOneShot(greenLightSound, volume);
                wasPlayedGreen = true;
                wasPlayedRed = false;
            }
        }
    }
}
